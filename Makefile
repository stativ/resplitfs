
CC?=gcc

CPPFLAGS+=-D_XOPEN_SOURCE=700 -D_DEFAULT_SOURCE

CFLAGS+=-std=c99 -Wall -pedantic
CFLAGS+=-O0 -g
CFLAGS+=$(shell pkg-config --cflags fuse)

LDFLAGS+=$(shell pkg-config --libs fuse)

resplitfs: context.h fsutils.h fsutils.c handlehash.h stringhash.h translatepath.h resplitfs.c
	$(CC) $(CPPFLAGS) $(CFLAGS) fsutils.c resplitfs.c $(LDFLAGS) -o resplitfs

.PHONY: all
all: resplitfs

.PHONY: test
test: all
	$(MAKE) -C test test

.PHONY: clean
clean:
	rm -f resplitfs
	$(MAKE) -C test clean
