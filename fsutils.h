/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2017 Lukas Jirkovsky <l.jirkovsky at gmail.com>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

#pragma once

/**
 * @brief Copy file to a temporary file.
 * 
 * @param source_ full source file path
 * @param[in,out] template_ target file template. The template is filled with real file name
 * @return 0 on success, -errno on error
 */
int resplitfs_copy_tmpfile(const char* source_, char* template_);

/**
 * @brief Copy directory to a temporary directory.
 * 
 * Recursively copies directory.
 * 
 * @param source_ source directory
 * @param[in,out] template_ target file template. The template is filled with real file name.
 * @return 0 on success, -errno on error
 */
int resplitfs_copy_tmpdir(const char* source_, char* template_);

/**
 * @brief Unlink (remove) directory.
 * 
 * Recursively removes directory and its contents.
 * 
 * @param dir_ directory to remove
 * @return 0 on success, -errno on error
 */
int resplitfs_unlink_dir(const char* dir_);
