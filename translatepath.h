/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 Lukas Jirkovsky <l.jirkovsky at gmail.com>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

#pragma once

#include <limits.h>
#include <string.h>

#include "context.h"
#include "pathmatch.h"

/**
 * @brief Translate path to a path in root
 * @param[out] out_ translated path
 * @param path_ path to translate
 */
__attribute__((always_inline)) inline void resplitfs_translate_path_root(char out_[PATH_MAX], const char* path_) {
	strcpy(out_, RESPLITFS_CONTEXT->root);
	strncat(out_, path_, PATH_MAX);
}

/**
 * @brief Translate path to a path in redirected
 * @param[out] out_ translated path
 * @param path_ path to translate
 */
__attribute__((always_inline)) inline void resplitfs_translate_path_redirected(char out_[PATH_MAX], const char* path_) {
	strcpy(out_, RESPLITFS_CONTEXT->redirected);
	strncat(out_, path_, PATH_MAX);
}

enum resplitfs_TranslatePathResult {
	TRES_ROOT,
	TRES_REDIR,
};

/**
 * @brief Translates a path to a full path with redirection
 * 
 * If the path_ contains any mask from RESPLITFS_CONTEXT->redirmatch, the "redirected"
 * path is used, otherwise the default path is used
 */
__attribute__((always_inline)) inline enum resplitfs_TranslatePathResult resplitfs_translate_path(char out_[PATH_MAX], const char* path_) {
	char** mask_ = RESPLITFS_CONTEXT->redirmatch;
	while(*mask_ != NULL) {
		if(pathmatch(path_, *mask_)) {
			resplitfs_translate_path_redirected(out_, path_);
			return TRES_REDIR;
		}
		++mask_;
	}
	resplitfs_translate_path_root(out_, path_);
	return TRES_ROOT;
}
