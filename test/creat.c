#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>

int main(int argc_, char* argv_[]) {
	if(argc_ != 2) {
		return 1;
	}
	chdir(argv_[1]);
	int res_ = open("creat_test", O_RDWR | O_CREAT, 0444);
	if (res_ < 0) {
		printf("result %d", res_);
		return res_;
	}
	return 0;
}
