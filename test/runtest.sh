#!/bin/sh

# Helper function to prepare testing environment
prepare() {
	DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
	pushd $DIR >/dev/null

	mkdir ./base
	mkdir ./redir
	mkdir ./target
	../resplitfs --match=.svn -b ./base -r ./redir ./target

	pushd target >/dev/null
}

# Helper function to tear down the testing environment
teardown() {
	popd >/dev/null

	fusermount -u  ./target
	rm -rf ./target
	rm -rf ./redir
	rm -rf ./base

	popd >/dev/null
}

# Run a single check
#   $1 test binary
runcheck() {
	prepare

	echo "Checking $1"
	../$1 `pwd`
	if [ $? -eq 0 ] ; then
		teardown
		echo "Check $1 OK"
	else
		teardown
		echo "Check $1 FAILED" >&2
		exit 1
	fi
}

# Run all tests
runcheck creat
exit 0
