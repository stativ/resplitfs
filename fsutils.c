/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2017 Lukas Jirkovsky <l.jirkovsky at gmail.com>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

#include "fsutils.h"

#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <fts.h>
#include <ftw.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/sendfile.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>

#include <pthread.h>

static pthread_mutex_t fts_read_mutex = PTHREAD_MUTEX_INITIALIZER;

static int ftw_remove_path(const char* fpath_, const struct stat* sb_, int typeflag_, struct FTW* ftwbuf_) {
    return remove(fpath_);
}

int resplitfs_copy_tmpfile(const char* source_, char* template_) {
	int in_ = open(source_, O_RDONLY);
	if (in_  == -1) {
		return -errno;
	}
	struct stat finfo_ = {0};
	fstat(in_, &finfo_);
	int out_ = mkstemp(template_);
	if (out_  == -1) {
		close(in_);
		return -errno;
	}
	ssize_t result_ = sendfile(out_, in_, NULL, finfo_.st_size);
	if(result_ != finfo_.st_size) {
		int errno_ = errno;
		close(in_);
		close(out_);
		return -errno_;
	}
	close(in_);
	// copy the file attributes and close the temp file
	// TODO: copy extended attributes
	fchown(out_, finfo_.st_uid, finfo_.st_gid);
	fchmod(out_, finfo_.st_mode);
	close(out_);
	return 0;
}

int resplitfs_copy_tmpdir(const char* source_, char* template_) {
	// create temporary directory
	if(mkdtemp(template_) != template_) {
		return -errno;
	}
	const char* tmpdir_ = template_;
	const size_t tmpdirlen_ = strlen(tmpdir_);
	
	char* const paths_[] = { strdup(source_), NULL };
	FTS* handle_ = fts_open(paths_, FTS_NOCHDIR | FTS_PHYSICAL, NULL);
	if (handle_ == 0) {
		free(paths_[0]);
		return -errno;
	}
	
	FTSENT* node_;
	// fts_read is not thread safe :(
	pthread_mutex_lock(&fts_read_mutex);
    while ((node_ = fts_read(handle_))) {
		// skip top level directory
		if(node_->fts_level == 0) {
			continue;
		}
		
		// construct destination path
		size_t dstpathlen_ = tmpdirlen_ + node_->fts_namelen + 2;
		FTSENT* parentnode_ = node_->fts_parent;
		while (parentnode_ != NULL && parentnode_->fts_level > 0) {
			if (parentnode_->fts_namelen > 0) {
				dstpathlen_ += parentnode_->fts_namelen + 1;
			}
			parentnode_ = parentnode_->fts_parent;
		}
		char* dstpath_ = malloc(sizeof(char) * dstpathlen_);
		dstpathlen_ -= 1;
		strcpy(dstpath_ + dstpathlen_ - node_->fts_namelen, node_->fts_name);
		dstpathlen_ -= node_->fts_namelen + 1;
		dstpath_[dstpathlen_] = '/';
		parentnode_ = node_->fts_parent;
		while (parentnode_ != NULL && parentnode_->fts_level > 0) {
			if (parentnode_->fts_namelen > 0) {
				strncpy(dstpath_ + dstpathlen_ - parentnode_->fts_namelen, parentnode_->fts_name, parentnode_->fts_namelen);
				dstpathlen_ -= parentnode_->fts_namelen + 1;
				dstpath_[dstpathlen_] = '/';
			}
			parentnode_ = parentnode_->fts_parent;
		}
		strncpy(dstpath_, tmpdir_, tmpdirlen_);
		
		switch (node_->fts_info) {
			// directory
			case FTS_D: {
				if (mkdir(dstpath_, node_->fts_statp->st_mode) != 0) {
					free(dstpath_);
					goto ret_errno;
				}
				break;
			}
			// file
			case FTS_F: {
				int in_ = open(node_->fts_accpath, O_RDONLY);
				if (in_  == -1) {
					free(dstpath_);
					goto ret_errno;
				}
				int out_ = open(dstpath_, O_WRONLY | O_CREAT, node_->fts_statp->st_mode);
				if (out_  == -1) {
					close(in_);
					free(dstpath_);
					goto ret_errno;
				}
				ssize_t result_ = sendfile(out_, in_, NULL, node_->fts_statp->st_size);
				if (result_ != node_->fts_statp->st_size) {
					close(in_);
					close(out_);
					free(dstpath_);
					goto ret_errno;
				}
				// TODO: copy extended attributes
				fchown(out_, node_->fts_statp->st_uid, node_->fts_statp->st_gid);
				fchmod(out_, node_->fts_statp->st_mode);
				close(out_);
				
				break;
			}
			// symbolic links
			case FTS_SL:
			case FTS_SLNONE: {
				char linktarget_[PATH_MAX];
				if (readlink(node_->fts_accpath, linktarget_, PATH_MAX) == 1) {
					free(dstpath_);
					goto ret_errno;
					break;
				}
				if (symlink(linktarget_, dstpath_) != 0) {
					free(dstpath_);
					goto ret_errno;
				}
				break;
			}
			default:
				break;
		}
		
		free(dstpath_);
	}
	// everything is OK
	if (errno == 0) {
		pthread_mutex_unlock(&fts_read_mutex);
		fts_close(handle_);
		free(paths_[0]);
		return 0;
	}
	
	int errno_;
ret_errno:
	pthread_mutex_unlock(&fts_read_mutex);
	errno_ = errno;
	fts_close(handle_);
	free(paths_[0]);
	// remove already copied files
	resplitfs_unlink_dir(tmpdir_);
	return -errno_;
}

int resplitfs_unlink_dir(const char* dir_) {
	return nftw(dir_, ftw_remove_path, 64, FTW_DEPTH | FTW_PHYS);
}
