/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 Lukas Jirkovsky <l.jirkovsky at gmail.com>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

#pragma once

#include <fuse.h>

#include "handlehash.h"

/**
 * @brief Structure storing the internal FUSE context information
 */
struct resplitfs_Context {
	char** redirmatch; // the array of strings that, if matches, selects path for redirection. The array is terminated by NULL
	char* root; // the root path where the filesystem is mounted
	char* redirected; // the path where the .svn files are redirected
	struct resplitfs_RedirectedHandle directoryhandlemap; //  maps the root directory handles to a corresponding handles in redirected
};

/**
 * @brief Access the context data
 */
#define RESPLITFS_CONTEXT ((struct resplitfs_Context *) fuse_get_context()->private_data)
