/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 Lukas Jirkovsky <l.jirkovsky at gmail.com>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

#pragma once

#include <stdlib.h>
#include <string.h>

#include <uthash.h>

struct resplitfs_StringHash_Entry {
	char* key;
	UT_hash_handle hh;
};

struct resplitfs_StringHash {
	struct resplitfs_StringHash_Entry* table;
};

/**
 * @brief Construct a new StringHash table.
 * @return new table
 */
__attribute__((always_inline)) inline struct resplitfs_StringHash resplitfs_StringHash_construct() {
	struct resplitfs_StringHash table_ = {
		.table = NULL
	};
	return table_;
}

/**
 * @brief Destroy StringHash table represented by its head.
 */
__attribute__((always_inline)) inline void resplitfs_StringHash_destroy(struct resplitfs_StringHash* table_) {
	struct resplitfs_StringHash_Entry* current_;
	struct resplitfs_StringHash_Entry* tmp_;
	HASH_ITER(hh, table_->table, current_, tmp_) {
		HASH_DEL(table_->table, current_);
		free(current_->key);
		free(current_);
	}
}

/**
 * @brief Allocate and fill a new StringHash table.
 * @param table_ table to use
 * @param string_ value to store
 */
__attribute__((always_inline)) inline void resplitfs_StringHash_insert(struct resplitfs_StringHash* table_, const char* string_) {
	struct resplitfs_StringHash_Entry* hashentry_ = malloc(sizeof(struct resplitfs_StringHash_Entry));
	hashentry_->key = strdup(string_);
	HASH_ADD_KEYPTR(hh, table_->table, hashentry_->key, strlen(hashentry_->key), hashentry_);
}

/**
 * @brief Check whether a StringHash table contains a given key.
 * @param table_ table to use
 * @param string_ key to check
 */
__attribute__((always_inline)) inline _Bool resplitfs_StringHash_contains(struct resplitfs_StringHash* table_, const char* string_) {
	struct resplitfs_StringHash_Entry* s_;
	HASH_FIND_STR(table_->table, string_, s_);
	return s_ != 0;
}
