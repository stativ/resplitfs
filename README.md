# ReSplitFS #
A simple FUSE-based filesystem that redirects all operations to two
different underlying directories based on path component matching.

If an accessed path contains the specified component, it is redirected to
one directory. If it doesn't contain the component, it is redirected to
another directory.

This is useful eg. when you want (or need) to keep VCS repository (such as .hg, .git or .svn) separate from the sources.

## Requirements ##
* [FUSE](https://github.com/libfuse/libfuse) with API version > 26
* [uthash](http://troydhanson.github.io/uthash/)
* gcc

## Compilation ##
* run `make`

## Usage ##

See `resplitfs --help` output.
