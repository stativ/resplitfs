/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 Lukas Jirkovsky <l.jirkovsky at gmail.com>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

/*
 * Resources:
 *   http://www.cs.nmsu.edu/~pfeiffer/fuse-tutorial/
 *   https://www.cs.hmc.edu/~geoff/classes/hmc.cs135.201109/homework/fuse/fuse_doc.html
 */

#define FUSE_USE_VERSION 26

#include <fuse.h>

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/limits.h>
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <sys/types.h>
#include <sys/xattr.h>
#include <time.h>
#include <unistd.h>

#include <uthash.h>

#include "context.h"
#include "fsutils.h"
#include "handlehash.h"
#include "stringhash.h"
#include "translatepath.h"

static int resplitfs_make_parent_path(char* path_) {
	char* p_;
	for (p_ = strchr(path_+1, '/'); p_ != NULL; p_=strchr(p_+1, '/')) {
		*p_='\0';
		if (mkdir(path_, S_IRWXU) == -1 && errno!=EEXIST) { // TODO: copy the mode from the root tree
			*p_='/';
			return -1;
		}
		*p_='/';
	}
	return 0;
}

/**
 * @brief Check if path points to a directory
 * @return true if the path is a directory
 */
bool resplitfs_isdir(const char *path_) {
	struct stat st_;
	if (stat(path_, &st_) != 0)
		return false;
	return S_ISDIR(st_.st_mode);
}

int resplitfs_getattr(const char* path_, struct stat* statbuf_) {
	char fpath_[PATH_MAX];
	resplitfs_translate_path(fpath_, path_);
	return lstat(fpath_, statbuf_) == 0 ? 0 : -errno;
}

int resplitfs_readlink(const char* path_, char* link_, size_t size_) {
	char fpath_[PATH_MAX];
	resplitfs_translate_path(fpath_, path_);
	return readlink(fpath_, link_, size_ - 1)  != -1 ? 0 : -errno;
}

int resplitfs_mknod(const char* path_, mode_t mode_, dev_t dev_) {
	char fpath_[PATH_MAX];
	// default dirs doesn't need any special handling
	if(resplitfs_translate_path(fpath_, path_) == TRES_ROOT) {
		return mknod(fpath_, mode_, dev_)  == 0 ? 0 : -errno;
	}
	else {
		// for redirected, fir try to run the operation, if that fails because
		// parent doesn't exist, create the parent
		int res_ = mknod(fpath_, mode_, dev_);
		if(res_ != 0 && errno == ENOENT) {
			// create parent directory
			resplitfs_make_parent_path(fpath_);
			// try to run the operation once again
			return mknod(fpath_, mode_, dev_)  == 0 ? 0 : -errno;
		}
		return 0;
	}
}

int resplitfs_mkdir(const char* path_, mode_t mode_) {
	char fpath_[PATH_MAX];
	if(resplitfs_translate_path(fpath_, path_) == TRES_ROOT) {
		return mkdir(fpath_, mode_|S_IFDIR)  == 0 ? 0 : -errno;
	}
	else {
		// for redirected, fir try to run the operation, if that fails because
		// parent doesn't exist, create the parent
		int res_ = mkdir(fpath_, mode_|S_IFDIR);
		if(res_ != 0 && errno == ENOENT) {
			// create parent directory
			resplitfs_make_parent_path(fpath_);
			// try to run the operation once again
			return mkdir(fpath_, mode_|S_IFDIR)  == 0 ? 0 : -errno;
		}
		return 0;
	}
}

int resplitfs_unlink(const char* path_) {
	char fpath_[PATH_MAX];
	resplitfs_translate_path(fpath_, path_);
	return unlink(fpath_)  == 0 ? 0 : -errno;
}

int resplitfs_rmdir(const char* path_) {
	char fpath_[PATH_MAX];
	if(resplitfs_translate_path(fpath_, path_) == TRES_ROOT) {
		int res_ = rmdir(fpath_);
		if(res_ != 0) {
			return -errno;
		}
		
		// we may have to remove the redirected directory, too
		char redirpath_[PATH_MAX];
		resplitfs_translate_path_redirected(redirpath_, path_);
		res_ = rmdir(redirpath_);
		if(res_ != 0 && errno != ENOENT) {
			return -errno;
		}
		return 0;
	}
	return rmdir(fpath_)  == 0 ? 0 : -errno;
}

int resplitfs_symlink(const char* path_, const char* link_) {
	char flink_[PATH_MAX];
	resplitfs_translate_path(flink_, link_);
	return symlink(path_, flink_)  == 0 ? 0 : -errno;
}

int resplitfs_rename(const char* path_, const char* newpath_) {
	// TODO: maybe handle parent directories
	char fpath_[PATH_MAX];
	char fnewpath_[PATH_MAX];
	resplitfs_translate_path(fpath_, path_);
	enum resplitfs_TranslatePathResult newres_ = resplitfs_translate_path(fnewpath_, newpath_);
	int res_ = rename(fpath_, fnewpath_);
	if (res_ != 0) {
		// if the rename fails because the source and destination are not
		// on the same filesystem, fall back to copying file
		if (errno == EXDEV) {
			struct stat finfo_ = {0};
			lstat(fpath_, &finfo_);
			
			// template for temporary files
			char tmppath_[PATH_MAX];
			if(newres_ == TRES_REDIR) {
				resplitfs_translate_path_redirected(tmppath_, "/XXXXXX");
			}
			else {
				resplitfs_translate_path_root(tmppath_, "/XXXXXX");
			}
			
			// regular file
			if (S_ISREG(finfo_.st_mode)) {
				// copy the file to a temporary file
				if(resplitfs_copy_tmpfile(fpath_, tmppath_) != 0) {
					return -EXDEV;
				}

				// rename the temp file to the destination file and remove the original
				res_ = rename(tmppath_, fnewpath_);
				if(res_ != 0) {
					unlink(tmppath_);
					return -EXDEV;
				}
				unlink(fpath_);
			}
			// link
			else if (S_ISLNK(finfo_.st_mode)) {
				char linktarget_[PATH_MAX];
				if (readlink(fpath_, linktarget_, PATH_MAX) != 0) {
					return -EXDEV;
				}
				if (symlink(linktarget_, fnewpath_) != 0) {
					return -EXDEV;
				}
			}
			// directory
			else if (S_ISDIR(finfo_.st_mode)) {
				// copy the directory to a temporary directory
				if(resplitfs_copy_tmpdir(fpath_, tmppath_) != 0) {
					return -EXDEV;
				}

				// rename the temp file to the destination file and remove the original
				res_ = rename(tmppath_, fnewpath_);
				if(res_ != 0) {
					resplitfs_unlink_dir(tmppath_);
					return -EXDEV;
				}
				resplitfs_unlink_dir(fpath_);
			}
			else {
				return -EXDEV;
			}
		}
		else {
			return -errno;
		}
	}
	return 0;
}

int resplitfs_link(const char* path_, const char* newpath_) {
	char fpath_[PATH_MAX];
	char fnewpath_[PATH_MAX];
	resplitfs_translate_path(fpath_, path_);
	resplitfs_translate_path(fnewpath_, newpath_);
	return link(fpath_, fnewpath_)  == 0 ? 0 : -errno;
}

int resplitfs_chmod(const char* path_, mode_t mode_) {
	char fpath_[PATH_MAX];
	resplitfs_translate_path(fpath_, path_);
	return chmod(fpath_, mode_)  == 0 ? 0 : -errno;
}

int resplitfs_chown(const char* path_, uid_t uid_, gid_t gid_) {
	char fpath_[PATH_MAX];
	resplitfs_translate_path(fpath_, path_);
	return chown(fpath_, uid_, gid_)  == 0 ? 0 : -errno;
}

int resplitfs_truncate(const char* path_, off_t size_) {
	char fpath_[PATH_MAX];
	resplitfs_translate_path(fpath_, path_);
	return truncate(fpath_, size_)  == 0 ? 0 : -errno;
}

int resplitfs_open(const char* path_, struct fuse_file_info* finfo_) {
	char fpath_[PATH_MAX];
	resplitfs_translate_path(fpath_, path_);
	int fd_ = open(fpath_, finfo_->flags);
	finfo_->fh = fd_;
	return fd_ != -1 ? 0 : -errno;
}

int resplitfs_read(const char* path_, char* buf_, size_t size_, off_t offset_, struct fuse_file_info* finfo_) {
	ssize_t res_ = pread(finfo_->fh, buf_, size_, offset_);
	return (res_ >= 0) ? res_ : -errno;
}

int resplitfs_write(const char* path_, const char* buf_, size_t size_, off_t offset_, struct fuse_file_info* finfo_) {
	ssize_t res_ = pwrite(finfo_->fh, buf_, size_, offset_);
	return (res_ >= 0) ? res_ : -errno;
}

int resplitfs_statfs(const char* path_, struct statvfs* statvfs_) {
	char fpath_[PATH_MAX];
	resplitfs_translate_path(fpath_, path_);
	return statvfs(fpath_, statvfs_)  == 0 ? 0 : -errno;
}

int resplitfs_flush(const char* path_, struct fuse_file_info* fi_) {
	return 0;
}

int resplitfs_release(const char* path_, struct fuse_file_info* fi_) {
	return close(fi_->fh)  == 0 ? 0 : -errno;
}

int resplitfs_fsync(const char* path_, int datasync_, struct fuse_file_info* fi_) {
	if(datasync_) {
		return fdatasync(fi_->fh)  == 0 ? 0 : -errno;
	}
	else {
		return fsync(fi_->fh)  == 0 ? 0 : -errno;
	}
}
int resplitfs_setxattr(const char* path_, const char* name_, const char* value_, size_t size_, int flags_) {
	char fpath_[PATH_MAX];
	resplitfs_translate_path(fpath_, path_);
	return lsetxattr(fpath_, name_, value_, size_, flags_)  == 0 ? 0 : -errno;
}

int resplitfs_getxattr(const char* path_, const char* name_, char* value_, size_t size_) {
	char fpath_[PATH_MAX];
	resplitfs_translate_path(fpath_, path_);
	return lgetxattr(fpath_, name_, value_, size_) != -1 ? 0 : -errno;
}

int resplitfs_listxattr(const char* path_, char* list_, size_t size_) {
	char fpath_[PATH_MAX];
	resplitfs_translate_path(fpath_, path_);
	return llistxattr(fpath_, list_, size_) != -1 ? 0 : -errno;
}

int resplitfs_removexattr(const char* path_, const char* name_) {
	char fpath_[PATH_MAX];
	resplitfs_translate_path(fpath_, path_);
	return lremovexattr(fpath_, name_)  == 0 ? 0 : -errno;
}

int resplitfs_opendir(const char* path_, struct fuse_file_info* fi_) {
	char rootpath_[PATH_MAX];
	enum resplitfs_TranslatePathResult redirres_ = resplitfs_translate_path(rootpath_, path_);
	DIR* rootdp_ = opendir(rootpath_);
	if(rootdp_ == 0) {
		return -errno;
	}
	fi_->fh = (uint64_t)(intptr_t) rootdp_;
	
	// if the opened directory was from the root (ie. not redirected), we must try to open
	// the redirected path, too
	if(redirres_ == TRES_ROOT) {
		char redirpath_[PATH_MAX];
		resplitfs_translate_path_redirected(redirpath_, path_);
		DIR* redirdp_ = opendir(redirpath_);
		if(redirdp_ != 0) {
			// store the handle to redirected in hashtable inside context
			resplitfs_RedirectedHandle_insert(&RESPLITFS_CONTEXT->directoryhandlemap, rootdp_, redirdp_);
		}
	}
	
	return 0;
}

int resplitfs_readdir(const char* path_, void* buf_, fuse_fill_dir_t filler_, off_t offset_, struct fuse_file_info* fi_) {
	DIR* rootdp_ = (DIR *)(intptr_t) fi_->fh;
	struct dirent* rootde_ = readdir(rootdp_);
	if(rootde_ == 0) {
		return 1;
	}
	
	// check if there is a redirected directory corresponding to the processed directory
	// if there is no such directory, we just read the requested directory
	if(!resplitfs_RedirectedHandle_contains(&RESPLITFS_CONTEXT->directoryhandlemap, rootdp_)) {
		do {
			if (filler_(buf_, rootde_->d_name, NULL, 0) != 0) {
				return -ENOMEM;
			}
		} while ((rootde_ = readdir(rootdp_)) != NULL);
		return 0;
	}
	// otherwise we have to handle deduplication
	else {
		// hashtable used for deduplicating entries
		struct resplitfs_StringHash nametable_ = resplitfs_StringHash_construct();
		
		// fill root
		do {
			resplitfs_StringHash_insert(&nametable_, rootde_->d_name);
			if (filler_(buf_, rootde_->d_name, NULL, 0) != 0) {
				resplitfs_StringHash_destroy(&nametable_);
				return -ENOMEM;
			}
		} while ((rootde_ = readdir(rootdp_)) != NULL);
		
		// fill from the redirected path if possible
		DIR* redirdp_ = resplitfs_RedirectedHandle_find(&RESPLITFS_CONTEXT->directoryhandlemap, rootdp_);
		if(redirdp_ != 0) {
			struct dirent* redirde_ = readdir(redirdp_);
			if(redirde_ == 0) {
				resplitfs_StringHash_destroy(&nametable_);
				return 1;
			}
			do {
				if(!resplitfs_StringHash_contains(&nametable_, redirde_->d_name)) {
					if (filler_(buf_, redirde_->d_name, NULL, 0) != 0) {
						resplitfs_StringHash_destroy(&nametable_);
						return -ENOMEM;
					}
				}
			} while ((redirde_ = readdir(redirdp_)) != NULL);
		}
		resplitfs_StringHash_destroy(&nametable_);
		return 0;
	}
}

int resplitfs_releasedir(const char* path_, struct fuse_file_info* fi_) {
	DIR* rootdp_ = (DIR *)(intptr_t) fi_->fh;
	if(closedir(rootdp_) == -1) {
		return  -errno;
	}
	
	// close redirected if needed
	DIR* redirdp_ = resplitfs_RedirectedHandle_erase(&RESPLITFS_CONTEXT->directoryhandlemap, rootdp_);
	if(redirdp_ != 0) {
		if(closedir(redirdp_) == -1) {
			return  -errno;
		}
	}
	
	return 0;
}

int resplitfs_fsyncdir(const char* path_, int datasyn_, struct fuse_file_info* fi_) {
	DIR* rootdp_ = (DIR *)(intptr_t) fi_->fh;
	int rootfd_ = dirfd(rootdp_);
	if(rootfd_ == -1) {
		return -errno;
	}
	if(fsync(rootfd_) == -1) {
		return -errno;
	}
	
	// sync redirected
	DIR* redirdp_ = resplitfs_RedirectedHandle_find(&RESPLITFS_CONTEXT->directoryhandlemap, rootdp_);
	if(redirdp_ != 0) {
		int redirfd_ = dirfd(redirdp_);
		if(redirfd_ == -1) {
			return -errno;
		}
		if(fsync(redirfd_) == -1) {
			return -errno;
		}
	}
	
	return 0;
}

void* resplitfs_init(struct fuse_conn_info *conn_) {
	return RESPLITFS_CONTEXT;
}

void resplitfs_destroy(void* userdata_) {
	struct resplitfs_Context* udata_ = (struct resplitfs_Context*) userdata_;
	char** tmp_ = udata_->redirmatch;
	while(tmp_ != NULL && *tmp_ != NULL) {
		free(*tmp_);
		++tmp_;
	}
	free(udata_->redirmatch);
	free(udata_->root);
	free(udata_->redirected);
	resplitfs_RedirectedHandle_destroy(&udata_->directoryhandlemap);
	free(udata_);
}

int resplitfs_access(const char* path_, int mask_) {
	char fpath_[PATH_MAX];
	resplitfs_translate_path(fpath_, path_);
	return access(fpath_, mask_)  == 0 ? 0 : -errno;
}

int resplitfs_create(const char* path_, mode_t mode_, struct fuse_file_info* fi_) {
	// pretty much the same as open, but passes mode_, too
	// this is used for calls such as open("foo", O_RDWR | O_CREAT, 0444)
	// where the standard split into mknod and open won't work due to missing write permission
	char fpath_[PATH_MAX];
	resplitfs_translate_path(fpath_, path_);
	int fd_ = open(fpath_, fi_->flags | O_CREAT, mode_);
	fi_->fh = fd_;
	return fd_ != -1 ? 0 : -errno;
}

int resplitfs_ftruncate(const char* path_, off_t offset_, struct fuse_file_info* fi_) {
	return ftruncate(fi_->fh, offset_)  == 0 ? 0 : -errno;
}

int resplitfs_fgetattr(const char* path_, struct stat* statbuf_, struct fuse_file_info* fi_) {
	char fpath_[PATH_MAX];
	resplitfs_translate_path(fpath_, path_);
	return fstat(fi_->fh, statbuf_) == 0 ? 0 : -errno;
}

/*int resplitfs_lock(const char* path_, struct fuse_file_info* fi_, int cmd,struct flock *) {
	
}*/

int resplitfs_utimens(const char* path_, const struct timespec tv_[2]) {
	char fpath_[PATH_MAX];
	resplitfs_translate_path(fpath_, path_);
	return utimensat(0, fpath_, tv_, 0)  == 0 ? 0 : -errno;
}

/*int resplitfs_bmap(const char* path_, size_t blocksize, uint64_t *idx) {
	
}

int resplitfs_ioctl(const char* path_, int cmd, void *arg, struct fuse_file_info* fi_, unsigned int flags, void *data) {
	
}

int resplitfs_poll(const char* path_, struct fuse_file_info* fi_, struct fuse_pollhandle *ph, unsigned *reventsp) {
	
}

int resplitfs_write_buf(const char* , struct fuse_bufvec *buf, off_t off, struct fuse_file_info* fi_) {
	
}

int resplitfs_read_buf(const char* , struct fuse_bufvec **bufp, size_t size, off_t off, struct fuse_file_info* fi_) {
	
}

int resplitfs_flock(const char* path_, struct fuse_file_info* fi_, int op) {
	
}

int resplitfs_fallocate(const char* path_, int, off_t, off_t, struct fuse_file_info* fi_) {
	
}*/


struct fuse_operations resplitfs_oper = {
	.getattr = resplitfs_getattr,
	.readlink = resplitfs_readlink,
	.mknod = resplitfs_mknod,
	.mkdir = resplitfs_mkdir,
	.unlink = resplitfs_unlink,
	.rmdir = resplitfs_rmdir,
	.symlink = resplitfs_symlink,
	.rename = resplitfs_rename,
	.link = resplitfs_link,
	.chmod = resplitfs_chmod,
	.chown = resplitfs_chown,
	.truncate = resplitfs_truncate,
	.open = resplitfs_open,
	.read = resplitfs_read,
	.write = resplitfs_write,
	.statfs = resplitfs_statfs,
 	.flush = resplitfs_flush,
	.release = resplitfs_release,
	.fsync = resplitfs_fsync,
	.setxattr = resplitfs_setxattr,
	.getxattr = resplitfs_getxattr,
	.listxattr = resplitfs_listxattr,
	.removexattr = resplitfs_removexattr,
	.opendir = resplitfs_opendir,
	.readdir = resplitfs_readdir,
	.releasedir = resplitfs_releasedir,
	.fsyncdir = resplitfs_fsyncdir,
	.init = resplitfs_init,
	.destroy = resplitfs_destroy,
	.access = resplitfs_access,
 	.create = resplitfs_create,
	.ftruncate = resplitfs_ftruncate,
 	.fgetattr = resplitfs_fgetattr,
// 	.lock
	.utimens = resplitfs_utimens,
// 	.bmap
// 	.ioctl
// 	.poll
// 	.write_buf
// 	.read_buf
// 	.flock
// 	.fallocate
};

enum {
	OPT_KEY_MATCH,
	OPT_KEY_HELP
};

static struct fuse_opt resplitfs_opts[] = {
	// long options
	FUSE_OPT_KEY("--match=", OPT_KEY_MATCH),
	{"--base=%s", offsetof(struct resplitfs_Context, root), 0 },
	{"--redir=%s", offsetof(struct resplitfs_Context, redirected), 0 },
	// shortcut options
	{"-b %s", offsetof(struct resplitfs_Context, root), 0 },
	{"-r %s", offsetof(struct resplitfs_Context, redirected), 0 },
	// default options
	FUSE_OPT_KEY("-h", OPT_KEY_HELP),
	FUSE_OPT_KEY("--help", OPT_KEY_HELP),
	FUSE_OPT_END
};

static int resplitfs_opt_proc(void *data_, const char *arg_, int key_, struct fuse_args *outargs_) {
	struct resplitfs_Context* userdata_ = (struct resplitfs_Context*) data_;
	switch (key_) {
		case OPT_KEY_MATCH: {
			size_t matchcount_ = 0;
			char** tmp_ = userdata_->redirmatch;
			while(tmp_ != NULL && *tmp_ != NULL) {
				++matchcount_;
				++tmp_;
			}
			char* match_ = strchr(arg_, '=');
			if(match_ == NULL || match_[1] == '\0') {
				fprintf(stderr, "Missing argument in %s\n", arg_);
				exit(1);
			}
			userdata_->redirmatch = realloc(userdata_->redirmatch, (matchcount_ + 2) * sizeof(char*));
			userdata_->redirmatch[matchcount_] = strdup(match_ + 1);
			userdata_->redirmatch[matchcount_ + 1] = NULL;
			return 0;
		}
		case OPT_KEY_HELP:
			fprintf(stderr,
			       "usage: %s mountpoint [options]\n"
			       "\n"
			       "general options:\n"
			       "    -o opt,[opt...]  mount options\n"
			       "    -h   --help      print help\n"
			       "\n"
			       "ReSpliFS options:\n"
			       "    --match=STRING\n"
			       "           String which, if present in path, selects path for redirection.\n"
				   "           May be specified multiple times.\n"
			       "           Default value: .svn.\n"
			       "\n"
			       "    -b PATH, --base=PATH\n"
			       "           Path where files are redirected by default.\n"
			       "\n"
			       "    -r PATH, --redir=PATH\n"
			       "           Path where files are redirected if their path contains match.\n"
			       "\n",
			       outargs_->argv[0]
			);
			fuse_opt_add_arg(outargs_, "-ho");
			fuse_main(outargs_->argc, outargs_->argv, &resplitfs_oper, NULL);
			exit(1);
	}
	return 1;
}

/**
 * @warning Beware, there is absolutelly no access checking
 */
int main(int argc_, char *argv_[]) {
	struct resplitfs_Context* userdata_ = malloc(sizeof(struct resplitfs_Context));
	userdata_->redirmatch = NULL;
	userdata_->root = NULL;
	userdata_->redirected = NULL;
	userdata_->directoryhandlemap = resplitfs_RedirectedHandle_construct();
	
	// parse options
	struct fuse_args args_ = FUSE_ARGS_INIT(argc_, argv_);
	if(fuse_opt_parse(&args_, userdata_, resplitfs_opts, resplitfs_opt_proc) != 0) {
		fprintf(stderr, "Failed to parse command line arguments!\n");
		exit(1);
	}
	
	// set default values
	if(userdata_->redirmatch == NULL) {
		userdata_->redirmatch = malloc(2 * sizeof(char*));
		userdata_->redirmatch[0] = NULL;
		userdata_->redirmatch[1] = NULL;
	}
	if(userdata_->redirmatch[0] == NULL) {
		userdata_->redirmatch[0] = strdup(".svn");
	}
	
	// check for validity
	if(userdata_->root == NULL) {
		fprintf(stderr, "Missing --base option!\n");
		exit(1);
	}
	if(userdata_->redirected == NULL) {
		fprintf(stderr, "Missing --redir option!\n");
		exit(1);
	}
	if(!resplitfs_isdir(userdata_->root)) {
		fprintf(stderr, "--base is not an existing directory!\n");
		exit(1);
	}
	if(!resplitfs_isdir(userdata_->redirected)) {
		fprintf(stderr, "--redir is not an existing directory!\n");
		exit(1);
	}
	
	// modify options to use realpath
	char* tmp_ = userdata_->root;
	userdata_->root = realpath(tmp_, NULL);
	free(tmp_);
	tmp_ = userdata_->redirected;
	userdata_->redirected = realpath(tmp_, NULL);
	free(tmp_);
	
	int res_ = fuse_main(args_.argc, args_.argv, &resplitfs_oper, userdata_);
	fuse_opt_free_args(&args_);
	return res_;
}
