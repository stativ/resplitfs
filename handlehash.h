/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 Lukas Jirkovsky <l.jirkovsky at gmail.com>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

#pragma once

#include <dirent.h>
#include <stdint.h>
#include <stdlib.h>

#include <pthread.h>

#include <uthash.h>

/**
 * @brief Stored the handle to a redirected directory for storage in hashtable
 */
struct resplitfs_RedirectedHandle_Entry {
	DIR* roothandle; // handle for the root directory, used as a key
	DIR* redirectedhandle; // handle for the redirected directory, the stored data
	UT_hash_handle hh; // makes the structure hashable using uthash
};

/**
 * @brief Hash map to store handles to redirected directories with the base directory handle as a key
 * 
 * The table is thread safe.
 */
struct resplitfs_RedirectedHandle {
	struct resplitfs_RedirectedHandle_Entry* map; // pointer to a head entry of the table
	int readercount; // the number of readers
	pthread_mutex_t readmutex; // mutex used by readers
	pthread_mutex_t readwritemutex; // global mutext for exclusion of readers/writers
};

/**
 * @brief Construct a new RedirectedHandle map.
 * @return new map
 */
__attribute__((always_inline)) inline struct resplitfs_RedirectedHandle resplitfs_RedirectedHandle_construct() {
	struct resplitfs_RedirectedHandle map_ = {
		.map = NULL,
		.readercount = 0,
		.readmutex = PTHREAD_MUTEX_INITIALIZER,
		.readwritemutex = PTHREAD_MUTEX_INITIALIZER
	};
	return map_;
}

/**
 * @brief Destroy RedirectedHandle map 
 * @param map_ pointer to map to destroy
 */
__attribute__((always_inline)) inline void resplitfs_RedirectedHandle_destroy(struct resplitfs_RedirectedHandle* map_) {
	pthread_mutex_destroy(&map_->readmutex);
	pthread_mutex_destroy(&map_->readwritemutex);
}

/**
 * @brief Lock map for reading
 * @param map_ map to use
 */
__attribute__((always_inline)) inline void resplitfs_RedirectedHandle_read_lock(struct resplitfs_RedirectedHandle* map_) {
	pthread_mutex_lock(&map_->readmutex);
	++map_->readercount;
	if(map_->readercount == 1) {
		pthread_mutex_lock(&map_->readwritemutex);
	}
	pthread_mutex_unlock(&map_->readmutex);
}

/**
 * @brief Unlock map for reading
 * @param map_ map to use
 */
__attribute__((always_inline)) inline void resplitfs_RedirectedHandle_read_unlock(struct resplitfs_RedirectedHandle* map_) {
	pthread_mutex_lock(&map_->readmutex);
	--map_->readercount;
	if(map_->readercount == 0) {
		pthread_mutex_unlock(&map_->readwritemutex);
	}
	pthread_mutex_unlock(&map_->readmutex);
}

/**
 * @brief Lock map for writing
 * @param map_ map to use
 */
__attribute__((always_inline)) inline void resplitfs_RedirectedHandle_write_lock(struct resplitfs_RedirectedHandle* map_) {
	pthread_mutex_lock(&map_->readwritemutex);
}

/**
 * @brief Unlock map for writing
 * @param map_ map to use
 */
__attribute__((always_inline)) inline void resplitfs_RedirectedHandle_write_unlock(struct resplitfs_RedirectedHandle* map_) {
	pthread_mutex_unlock(&map_->readwritemutex);
}

/**
 * @brief Add handle to a RedirectedHandle hash map.
 * @param map_ map to use
 * @param rootdp_ dirent structure of the base directory used as a key
 * @param redirdp_ dirent structure to store
 */
__attribute__((always_inline)) inline void resplitfs_RedirectedHandle_insert(struct resplitfs_RedirectedHandle* map_, DIR* rootdp_, DIR* redirdp_) {
	struct resplitfs_RedirectedHandle_Entry* redirhandle_ = malloc(sizeof(struct resplitfs_RedirectedHandle_Entry));
	redirhandle_->roothandle = rootdp_;
	redirhandle_->redirectedhandle = redirdp_;
	
	resplitfs_RedirectedHandle_write_lock(map_);
	HASH_ADD_PTR(map_->map, roothandle, redirhandle_);
	resplitfs_RedirectedHandle_write_unlock(map_);
}

/**
 * @brief Check whether a RedirectedHandle hash map contains a given key.
 * @param map_ map to use
 * @param rootdp_ key to check
 */
__attribute__((always_inline)) inline _Bool resplitfs_RedirectedHandle_contains(struct resplitfs_RedirectedHandle* map_, DIR* rootdp_) {
	struct resplitfs_RedirectedHandle_Entry* redirhandle_;
	
	resplitfs_RedirectedHandle_read_lock(map_);
	HASH_FIND_PTR(map_->map, &rootdp_, redirhandle_);
	resplitfs_RedirectedHandle_read_unlock(map_);
	
	return redirhandle_ != 0;
}

/**
 * @brief Find value for a given key in a RedirectedHandle hash map.
 * @param map_ map to use
 * @param rootdp_ key to find
 * @return the stored value
 */
__attribute__((always_inline)) inline DIR* resplitfs_RedirectedHandle_find(struct resplitfs_RedirectedHandle* map_, DIR* rootdp_) {
	struct resplitfs_RedirectedHandle_Entry* redirhandle_;
	
	resplitfs_RedirectedHandle_read_lock(map_);
	HASH_FIND_PTR(map_->map, &rootdp_, redirhandle_);
	resplitfs_RedirectedHandle_read_unlock(map_);
	
	if(redirhandle_ != 0) {
		return (DIR*) redirhandle_->redirectedhandle;
	}
	return 0;
}

/**
 * @brief Erase an entry from a RedirectedHandle hash map.
 * When a value is erased, the pointed directory is closed using closedir.
 * @param map_ map to use
 * @param rootdp_ key to erase
 * @return direntry of the erased value (the direntry doesn't need to be freed explicitly as documented in readdir(3))
 */
__attribute__((always_inline)) inline DIR* resplitfs_RedirectedHandle_erase(struct resplitfs_RedirectedHandle* map_, DIR* rootdp_) {
	struct resplitfs_RedirectedHandle_Entry* redirhandle_;
	
	resplitfs_RedirectedHandle_write_lock(map_);
	HASH_FIND_PTR(map_->map, &rootdp_, redirhandle_);
	if(redirhandle_ != 0) {
		DIR* redirdp_ = (DIR*) redirhandle_->redirectedhandle;
		HASH_DEL(map_->map, redirhandle_);
		resplitfs_RedirectedHandle_write_unlock(map_);
		free(redirhandle_);
		return redirdp_;
	}
	resplitfs_RedirectedHandle_write_unlock(map_);
	return 0;
}
