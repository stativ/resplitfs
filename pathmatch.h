/*
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2017 Lukas Jirkovsky <l.jirkovsky at gmail.com>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

#pragma once

/**
 * @brief Test whether path \a haystack_ contains needle_ component.
 * @param haystack_ path to test
 * @param needle_ component to search for
 * @return true if the \a haystack_ contains \a needle
 */
__attribute__((always_inline)) inline _Bool pathmatch(const char* haystack_, const char* needle_) {
	const char* h_ = haystack_;
	// first part of the path if the path is relative
	if(*h_ != '/') {
		const char* n_ = needle_;
		_Bool found_ = 1;
		while(*n_ != '\0') {
			if(*h_ == '\0') {
				return 0;
			}
			if(*h_ != *n_) {
				found_ = 0;
				h_++;
				break;
			}
			h_++;
			n_++;
		}
		if(found_ && (*h_ == '\0' || *h_ == '/')) {
			return 1;
		}
	}

	while(*h_ != '\0') {
		if(*h_ == '/') {
			h_++;
			const char* n_ = needle_;
			_Bool found_ = 1;
			while(*n_ != '\0') {
				if(*h_ == '\0') {
					return 0;
				}
				if(*h_ != *n_) {
					found_ = 0;
					break;
				}
				h_++;
				n_++;
			}
			if(found_ && (*h_ == '\0' || *h_ == '/')) {
				return 1;
			}
		}
		h_++;
	}
	return 0;
}
